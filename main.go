package main

import (
	"M3u8Downloader"
	"bufio"
	"fmt"
	"io"
	"log"
	ultis "m3u8download/pkg/utils"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

func downloadOneFile(url string, outputPath string) {
	// 创建一个HTTP GET请求
	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error fetching the URL:", err)
		return
	}
	defer response.Body.Close()

	// 检查响应状态码
	if response.StatusCode != http.StatusOK {
		fmt.Println("Error: unexpected status code", response.StatusCode)
		return
	}

	// 创建文件用于保存下载的内容
	file, err := os.Create(outputPath)
	if err != nil {
		fmt.Println("Error creating file:", err)
		return
	}
	defer file.Close()

	// 将HTTP响应体的内容复制到文件
	_, err = io.Copy(file, response.Body)
	if err != nil {
		fmt.Println("Error copying file:", err)
		return
	}

	fmt.Println("File downloaded successfully!")
}

func phraseM3U8File(filePath string) []string {
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}
	defer file.Close()

	var strSource []byte
	_, err = file.Read(strSource)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	var res []string
	sourceString := string(strSource)
	fmt.Printf("sourceString: %v\n", sourceString)
	for _, v := range strings.Split(sourceString, "\n") {
		url := strings.TrimSpace(v)
		if isValidURLString(url) {
			continue
		}
		res = append(res, url)
	}

	return res
}

func isValidURLString(URL string) bool {
	u, err := url.Parse(URL)
	if err != nil || u.Scheme == "" || u.Host == "" {
		return false
	}

	return true
}

func sanitizeFileName(input string) string {
	// 定义不允许的字符
	invalidChars := `\/:*?"<>|`

	// 替换不允许的字符为下划线
	sanitized := strings.Map(func(r rune) rune {
		if strings.ContainsRune(invalidChars, r) {
			return '_'
		}
		return r
	}, input)

	// 移除连续的下划线
	sanitized = strings.Join(strings.Fields(sanitized), "_")

	return sanitized
}

func main() {
	mergetOnly()
	// downloadedAndMerge()
}

func mergetOnly() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("请输入文件名称: ")
	scanner.Scan()
	movieName := scanner.Text()
	movieName = ultis.SanitizeFilename(movieName)

	fmt.Print("请输入文件夹路径: ")
	scanner.Scan()
	tmpFolder := scanner.Text()

	_, err := ultis.MergeVedioFile(tmpFolder, filepath.Join(filepath.Dir(tmpFolder), movieName))
	if err != nil {
		fmt.Print(`文件合成失败 : ` + err.Error())
	}

	fmt.Println("完成操作")
	fmt.Scanln()
}

func downloadedAndMerge() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("请输入文件名称: ")
	scanner.Scan()
	movieName := scanner.Text()
	movieName = ultis.SanitizeFilename(movieName)

	fmt.Print("输入文件url:")
	scanner.Scan()
	url := scanner.Text()

	tmpFolder := filepath.Join(`E:\spring`, ultis.GenerateRandomString(10))
	// 检查save文件夹
	err := ultis.CreateDirectory(tmpFolder)
	if err != nil {
		fmt.Print(`err: ` + err.Error())
		return
	}
	defer os.RemoveAll(tmpFolder)

	// 下载器
	m3u8 := M3u8Downloader.NewDownloader()
	m3u8.SetIfShowTheBar(true)
	m3u8.SetNumOfThread(16)

	m3u8.SetUrl(url)
	m3u8.SetSaveDirectory(tmpFolder)
	if err := m3u8.Download(); err != nil {
		log.Fatal(err.Error())
		return
	}
	fmt.Print(`文件下载成功！`)

	_, err = ultis.MergeVedioFile(tmpFolder, filepath.Join(filepath.Dir(tmpFolder), movieName))
	if err != nil {
		fmt.Print(`文件合成失败 : ` + err.Error())
	}

	fmt.Println("完成操作")
	fmt.Scanln()
}
